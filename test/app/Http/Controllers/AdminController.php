<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function getPosts(){
        return  Post::all();
    }
    public function getUsers(){
        return User::all();
    }
}
