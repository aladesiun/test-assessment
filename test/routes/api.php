<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\postController;
use App\Http\Controllers\managerController;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/posts', [postController::class,'index']);
Route::post('/post', [postController::class,'store']);
Route::get('/post/{id}', [postController::class,'show']);
Route::put('/post/edit/{id}', [postController::class,'update']);
Route::delete('/post/delete/{id}', [postController::class,'destroy']);

Route::get('/manager/posts', [managerController::class,'index']);

Route::get('/admin/posts', [AdminController::class,'getPosts']);
Route::get('/admin/users', [AdminController::class,'getUsers']);
